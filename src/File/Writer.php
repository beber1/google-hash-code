<?php 

namespace File;

class Writer
{
	private $file;

	public function __construct($file)
	{
		$this->file = fopen($file, 'w');
	}

	public function write($content)
	{
		fputs($this->file, trim($content).PHP_EOL);
	}
}