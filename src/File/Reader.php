<?php 

namespace File;

class Reader {
	
	private $file;

	public function __construct($file)
	{
		$this->file = fopen($file, 'r');
	}

	public function getFile()
	{
		return $this->file;
	}

	public function iterateFile()
	{
		while ($row = fgets($this->file)) {
			yield trim($row);
		}
	}
}