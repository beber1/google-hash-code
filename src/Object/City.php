<?php 

namespace Object;

use File\Reader;

class City extends Reader
{
	public $maps = [];
	public $cache = [];
	public $vehicles = [];

	public $currentStep = 0;
	public $steps;
	public $bonus;

	public function __construct($file)
	{
		parent::__construct($file);

		foreach ($this->iterateFile() as $index => $row) {
			$row = explode(' ', $row);

			if (0 === $index) {
				$this->generateMaps((int) $row[1], (int) $row[0]);
				$this->bonus = (int) $row[4];
				$this->steps = (int) $row[5];

				$this->createVehicule($row[2]);

				$this->maps[0][0] = $this->vehicles;
				continue;
			}

			$this->rides[] = new Ride([
				'start' => [(int) $row[0], (int) $row[1]],
				'target' => [(int) $row[2], (int) $row[3]],
				'minStart' => (int) $row[4],
				'lastStep' => (int) $row[5]
			]);
		}
	}

	public function simulate()
	{
		if ($this->steps === $this->currentStep) {
			return;
		}

		if (0 === count($this->rides)) {
			$return = true;
			foreach ($this->vehicles as $vehicule) {
				if ($vehicule->onRide) {
					$return = false;
				}
			}

			if ($return) {
				return;
			}
		} 

		foreach ( $this->rides as $index => $ride) {
			if ($vehicule = $this->getNearestvehicule($ride)) {
				$vehicule->addRide($index, $ride);
				$vehicule->onRide = true;
				unset($this->rides[$index]);
			}
		}

		foreach ($this->vehicles as $vehicule) {
			$vehicule->move();
		}

		$this->currentStep++;

		$this->simulate();
	}

	public function getNearestvehicule($ride)
	{
		$return = null;
		$max = -1;
		foreach ($this->vehicles as $vehicule) {
			if (!$vehicule->onRide) {
				$n = $this->getDistance($ride->start, [$vehicule->x, $vehicule->y]) - abs($ride->minStart - $this->currentStep);
				if ($max < $n) {
					$return = $vehicule;
					$max = $n;
				}
			}
		}

		return $return;
	}

	private function generateMaps($x, $y)
	{
		for($i=0;$i<$x;++$i) {
			for($j=0;$j<$y;++$j) {
				$this->maps[$i][$j] = 0;
			}
		}
	}

	public function getDistance($start, $end)
	{
		return abs(($end[0] - $start[0]) + ($end[1] - $start[1]));
	}

	public function createVehicule($number) {
        for($i=0;$i<$number;$i++)	 {
            array_push($this->vehicles, new Vehicule());
        }
    }
}