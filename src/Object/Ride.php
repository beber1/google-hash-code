<?php

namespace Object;

class Ride
{
	public $start;
	public $target;

	public $minStart;
	public $lastStep;

	public $started = false;
	public $done = false;

	public function __construct($data)
	{
		$this->start = $data['start'];
		$this->target = $data['target'];
		$this->minStart = $data['minStart'];
		$this->lastStep = $data['lastStep'];
	}
}