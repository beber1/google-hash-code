<?php 

namespace Object;

use File\Reader;

class Pizza extends Reader 
{
	const SLICE = 'S';
	const TOMATO = 'T';
	const MUSHROOM = 'M';

	private $cells = [];
	
	private $rows;
	private $columns;
	private $totalCells;

	private $minIngredient;
	private $maxCells;

	private $slices = [];

	private $sliceSchemes;

	public function __construct($file)
	{
		parent::__construct($file);

		foreach ($this->iterateFile() as $index => $row) {
			if (0 === $index) {
				list($this->rows, $this->columns, $this->minIngredient, $this->maxCells) = explode(' ', $row);

				$this->totalCells = (int) $this->rows * (int) $this->columns;
				continue;
			}

			$this->cells[] = str_split($row);
		}

		$this->sliceSchemes = $this->getSliceSchemes();
	}

	public function cut()
	{
		$this->generateSlices($slices);

		foreach ($slices as $slice) {
			if ($this->totalCells === $slice->getCellsCounter()) {
				return $this->formatSlice($slice);
			}
		}

		return null;				
	}

	private function formatSlice(Slice $slice) {
		$slices = [$slice->getSlice()];

		$slices = array_merge($slices, $this->findSlice($slice->getChildren()));

		return $slices;
	}

	private function findSlice($slices, $returnedSlices = []) {
		foreach ($slices as $slice) {
			if ($this->totalCells !== $slice->getCellsCounter()) {
				continue;
			}
			
			$returnedSlices[] = $slice->getSlice();

			if (0 === count($slice->getChildren())) {
				return $returnedSlices;
			}

			return $this->findSlice($slice->getChildren(), $returnedSlices);
		}
	}

	private function generateSlices(array &$slices = null)
	{
		if (null === $slices) {
			$slices = [];
			foreach ($this->sliceSchemes as $scheme) {
				$cells = $this->cells;
				$slices = array_merge($slices, $this->getSlicesFromScheme($cells, $scheme));
			}

			$this->generateSlices($slices);
		} else {
			foreach ($slices as $slice) {
				foreach ($this->sliceSchemes as $scheme) {
					$cells = $slice->getCells();
					$slice->setChildren(array_merge($slice->getChildren(), $this->getSlicesFromScheme($cells, $scheme, $slice)));
				}

				$this->generateSlices($slice->getChildren());
			}
		}
	}

	private function getSlicesFromScheme(array $cells, $scheme, Slice $parent = null)
	{
		$slices = [];
		foreach ($cells as $y => $rows) {
			foreach ($rows as $x => $cell) {
				if ($slice = $this->createSlice($cells, $x, $y, $scheme, $parent)) {
					$slices[] = $slice;
				}
			}
		}

		usort($slices, function (Slice $sliceA, Slice $sliceB) {
			return $sliceA->getScore() < $sliceB->getScore() ? -1 : 1;
		});
		
		return $slices;
	}

	private function createSlice(array $cells, $x, $y, array $scheme, Slice $parent = null) 
	{
		$ingrediends = [
			self::TOMATO => 0,
			self::MUSHROOM => 0
		];

		for ($i=$x;$i<$x+$scheme[0];++$i) {
			for ($j=$y;$j<$y+$scheme[1];++$j) {
				if (isset($cells[$j][$i]) && self::SLICE !== $cells[$j][$i]) {
					++$ingrediends[$cells[$j][$i]];
					$cells[$j][$i] = self::SLICE;
				}
			}
		}

		if (
			$scheme[0]*$scheme[1] === array_sum($ingrediends)
			&& $ingrediends[self::TOMATO] >= $this->minIngredient
			&& $ingrediends[self::MUSHROOM] >= $this->minIngredient
		) {
			return new Slice([
				'cells' => $cells,
				'coords' => [$x, $y],
				'scheme' => $scheme,
				'score' => ($scheme[0]*$scheme[1]*$this->minIngredient*2) + $score,
				'parent' => $parent
			]);
		}

		return false;
	}

	private function isFinish($cells) {
		foreach ($cells as $rows) {
			if (in_array(self::TOMATO, $rows) || in_array(self::MUSHROOM, $rows)) {
				return false;
			}
		}

		return true;
	}

	private function getSliceSchemes()
	{
		$sliceSchemes = [];
		for ($m=$this->maxCells;$m>=2;--$m) {
			for ($i=1;$i<=$m;++$i) {
				$n = $m/$i;
				if (!is_int($n)) {
					continue;
				}
				if (!in_array([$i,$n], $sliceSchemes) && $i <= $this->columns && $n <= (int) $this->rows) {
					$sliceSchemes[] = [$i,$n];
				}

				if (!in_array([$n,$i], $sliceSchemes) && $i <= $this->rows && $n <= (int) $this->columns) {
					$sliceSchemes[] = [$n,$i];
				}
			}
		}

		return $sliceSchemes;
	}
}