<?php

namespace Object;

class Vehicule
{
    public $x ;
    public $y ;
    public $xFinal ;
    public $yFinal ;
    public $xDirection ;
    public $yDirection ;
    public $rides = [] ;
    public $onRide = false;
    public $onStart = false ;


    public function __construct() {
        $this->x = 0 ;
        $this->y = 0 ;
    }

    public function addRide($index, $data) {
        $this->rides[$index] = $data;

        if ( $this->x == $data->start[0] && $this->y == $data->start[1] ) {
            $this->onStart = true;
        }
    }

    public function getDirections($x,$y,$xFinal,$yFinal) {
        if($yFinal-$y < 0 ) {
            return 'N' ;
        }
        if($yFinal-$y > 0 ) {
            return 'S' ;
        }

        if($xFinal-$x < 0 ) {
            return 'O' ;
        }
        if($xFinal-$x >0 ) {
            return 'E' ;
        }
    }

    public function changePosition($data) {
        switch($this->getDirections( $this->x, $this->y, $data[0], $data[1])) {
            case 'N' :
                $this->y-- ;
                break ;
            case 'S' :
                $this->y++ ;
                break ;
            case 'E' :
                $this->x++ ;
                break ;
            case 'O' :
                $this->x-- ;
                break ;
        }
    }

    public function move() {
        //dans le cas ou il a une route et qu'il est start
        if(count($this->rides)> 0 ) {
             $ride = end($this->rides) ;

             //si start
             if(!$this->onStart) {
                 $this->changePosition($ride->start);
             } else {
                 $this->changePosition($ride->target);
             }


            if(!$this->onStart) {
                if ( $this->x == $ride->start[0] && $this->y == $ride->start[1] ) {
                    $this->onStart = true ;
                }
            } else {
                if ( $this->x == $ride->target[0] && $this->y == $ride->target[1] ) {
                    $this->onStart = false ;
                    $this->onRide = false ; 
                }
            }

        }
    }

    public function getCurrentRide() {
        return $this->rides[count($this->rides)] ;
    }

}