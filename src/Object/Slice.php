<?php 

namespace Object;

class Slice
{
	private $cells = [];
	private $x;
	private $y;
	private $scheme;
	private $score;
	private $parent;
	private $children = [];
	private $cellsCounter = 0;

	public function __construct($data)
	{
		$this->cells = $data['cells'];
		$this->x = $data['coords'][0];
		$this->y = $data['coords'][1];
		$this->scheme = $data['scheme'];
		$this->score = $data['score'];
		$this->parent = $data['parent'];

		$this->setCellsCounter($this->countSCells($this->cells));
	}

	private function countSCells(array $cells) {
		$n = 0;
		foreach ($cells as $row) {
			$n += count(array_keys($row, Pizza::SLICE));
		}

		return $n;
	}

	public function setChildren(array $slices)
	{
		$this->children = $slices;

		return $this;
	}

	public function getChildren()
	{
		return $this->children;
	}

	public function getCells()
	{
		return $this->cells;
	}

	public function getSlice()
	{
		return [$this->y, $this->x, $this->y + $this->scheme[1] - 1, $this->x + $this->scheme[0] - 1];
	}

	public function getScore()
	{
		return $this->score;
	}

	public function setCellsCounter($cellsCounter)
	{
		if ($this->cellsCounter < $cellsCounter) {
			$this->cellsCounter = $cellsCounter;

			if (null !== $this->parent) {
				$this->parent->setCellsCounter($this->cellsCounter);
			}
		}
	}

	public function getCellsCounter()
	{
		return $this->cellsCounter;
	}
}