<?php

$loader = require 'vendor/autoload.php';

use Object\City;
use File\Writer;

$city = new City('./input/b_should_be_easy.in');

$city->simulate();

$writer = new Writer('./output/b_should_be_easy.out');
foreach ($city->vehicles as $i => $vehicle){
	$row = $i+1;
	foreach ($vehicle->rides as $j => $ride) {
		$row .= ' '.$j;
	}
	$writer->write($row);
}